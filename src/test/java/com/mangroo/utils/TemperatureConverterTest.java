package com.mangroo.utils;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import static org.assertj.core.api.Assertions.assertThat;

public class TemperatureConverterTest {

    @Test
    public void shouldConvertFToC() {

        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.HALF_UP);

        String result = df.format(TemperatureConverter.toCentegrade(32f));
        assertThat(result).isEqualTo("0.00");

        result = df.format(TemperatureConverter.toCentegrade(70f));
        assertThat(result).isEqualTo("21.11");

        result = df.format(TemperatureConverter.toCentegrade(0f));
        assertThat(result).isEqualTo("-17.78");
    }

    @Test
    public void shouldConvertCToF() {
        DecimalFormat df = new DecimalFormat("0.00");
        df.setRoundingMode(RoundingMode.HALF_UP);

        String result = df.format(TemperatureConverter.toFahrenheit(100f));
        assertThat(result).isEqualTo("87.56");

        result = df.format(TemperatureConverter.toFahrenheit(0f));
        assertThat(result).isEqualTo("32.00");

        result = df.format(TemperatureConverter.toFahrenheit(-10f));
        assertThat(result).isEqualTo("26.44");
    }

}
